-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2015 at 09:04 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `medicine_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `medicine_user_tb`
--

CREATE TABLE IF NOT EXISTS `medicine_user_tb` (
  `user_id` varchar(20) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_phone` varchar(20) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `shop_no` varchar(10) NOT NULL,
  `user_password` varchar(20) NOT NULL,
  `user_address` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medicine_user_tb`
--

INSERT INTO `medicine_user_tb` (`user_id`, `user_name`, `user_phone`, `user_type`, `shop_no`, `user_password`, `user_address`) VALUES
('med01', 'islam', '01914441334', 'admin', '01', '123', 'dhanmondi,dhaka,bangladesh'),
('man1isl', 'rakib', '01710001337', 'manager', '1', 'isl01rak1', 'Naogaon,Rajshahi'),
('sho1rak', 'mutasim', '01717252610', 'shop_keppe', '1', 'rakslmut1', 'Mymonshing,Dhaka'),
('dil1rak', 'Munna', '017100000', 'diller', '1', 'rakslMun1', 'Rajshahi,Bangladesh'),
('man2isl', 'bumba', '01717890134', 'manager', '2', 'isl01bum2', 'Narayongonj,Dhaka'),
('dil1rak', 'shofiq', '0181000000', 'diller', '1', 'rakslsho1', 'soriyotpur,dhaka'),
('dil1rak', 'amir', '016700000', 'diller', '1', 'rakslami1', 'soriyotpur,dhaka'),
('dil1rak', 'nur', '01717252610', 'diller', '1', 'rakslnur1', 'madaripur,dhaka'),
('dil1rak', 'shohag', '017896', 'diller', '1', 'rakslsho1', 'soriyotpur,dhaka'),
('dil1rak', 'harun', '0123', 'diller', '1', 'rakslhar1', 'soriyotpur,dhaka'),
('sho1rak', 'runa', '01717890134', 'shop_keppe', '1', 'rakslrun1', 'dhaka'),
('sho2isl', 'pavel', '01717890134', 'shop_kepper', '2', 'isl01pav2', 'dhaka'),
('med02', 'manager', '0170000000', 'manager', '01', '12', 'dhaka'),
('med03', 'shop_kepper', '01717456987', 'shop_kepper', '01', '1', 'dhaka'),
('sho01man', 'easha', '01944723020', 'shop_kepper', '01', 'man02eas01', 'dhaka'),
('man03isl', 'Rashib', '0123456', 'manager', '03', 'isl01Ras03', 'dhaka'),
('sho03isl', 'rony', '0147', 'shop_kepper', '03', 'isl01ron03', 'dhaka'),
('sho03Ras', 'moli', '0123654', 'shop_kepper', '03', 'Rasslmol03', 'dhaka'),
('man03isl', 'saddam', '01455', 'manager', '03', 'isl01sad03', 'dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `product_storage_tb`
--

CREATE TABLE IF NOT EXISTS `product_storage_tb` (
  `product_storage_host_id` varchar(30) NOT NULL,
  `product_storage_shop_no` varchar(20) NOT NULL,
  `product_storage_name` varchar(30) NOT NULL,
  `product_storage_quantity` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_storage_tb`
--

INSERT INTO `product_storage_tb` (`product_storage_host_id`, `product_storage_shop_no`, `product_storage_name`, `product_storage_quantity`) VALUES
('N#01', '01', 'Napa', '500'),
('P#01', '01', 'Peracitamol', '500'),
('A#01', '01', 'Antacid', '200'),
('N#02', '01', 'med_A', '500'),
('N#03', '01', 'med_B', '1000'),
('N#04', '01', 'med_C', '200'),
('N#05', '01', 'med_D', '100'),
('N#06', '01', 'med_DD', '50'),
('N#07', '01', 'med_AA', '100'),
('N#08', '01', 'med_AB', '100'),
('N#09', '01', 'med_AC', '50'),
('N#10', '01', 'med_AZ', '100'),
('N#11', '01', 'med_B12', '100'),
('N#12', '01', 'med_A11', '20'),
('N#20', '01', 'med_A20', '100'),
('A#21', '01', 'Seclo', '10'),
('A#23', '01', 'med_A23', '20'),
('d#03', '03', 'Sedil', '20');

-- --------------------------------------------------------

--
-- Table structure for table `product_tb`
--

CREATE TABLE IF NOT EXISTS `product_tb` (
  `product_host_id` varchar(30) NOT NULL,
  `product_host_name` varchar(50) NOT NULL,
  `product_sl` varchar(20) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `company_name` varchar(50) NOT NULL,
  `buy_price` varchar(20) NOT NULL,
  `sell_price` varchar(20) NOT NULL,
  `exp_date` varchar(20) NOT NULL,
  `mfg_date` varchar(20) NOT NULL,
  `quantity` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_tb`
--

INSERT INTO `product_tb` (`product_host_id`, `product_host_name`, `product_sl`, `product_name`, `company_name`, `buy_price`, `sell_price`, `exp_date`, `mfg_date`, `quantity`) VALUES
('med01', 'islam', 'N#01', 'Napa', 'Opsonin', '450', '500', '2015-09-12', '2017-09-12', '500'),
('med02', 'manager', 'P#01', 'Peracitamol', 'Acme', '250', '500', '2015-09-12', '2017-09-12', '500'),
('med01', 'islam', 'A#01', 'Antacid', 'Square', '150', '200', '2015-09-13', '2016-01-04', '200'),
('med02', 'manager', 'N#02', 'med_A', 'Square', '200', '250', '2015-09-12', '2017-09-12', '500'),
('med02', 'manager', 'N#03', 'med_B', 'Acme', '500', '750', '2015-09-12', '2016-09-12', '1000'),
('med02', 'manager', 'N#04', 'med_C', 'Square', '400', '600', '2015-09-12', '2017-09-12', '200'),
('med02', 'manager', 'N#05', 'med_D', 'Opsonin', '50', '100', '2015-09-12', '2015-09-12', '100'),
('med02', 'manager', 'N#06', 'med_DD', 'drug', '400', '500', '2015-09-12', '2015-09-12', '50'),
('med02', 'manager', 'N#07', 'med_AA', 'Acme', '190', '200', '2015-09-12', '2015-09-12', '100'),
('med02', 'manager', 'N#08', 'med_AB', 'Acme', '190', '250', '2015-10-12', '2017-09-12', '100'),
('med02', 'manager', 'N#09', 'med_AC', 'Opsonin', '7000', '8000', '2015-09-12', '2017-09-12', '50'),
('med02', 'manager', 'N#10', 'med_AZ', 'drug', '500', '600', '2015-09-12', '2017-09-12', '100'),
('med02', 'manager', 'N#11', 'med_B12', 'Opsonin', '400', '500', '2015-09-12', '2017-09-12', '100'),
('med02', 'manager', 'N#12', 'med_A11', 'Square', '10', '15', '2015-09-12', '2017-09-12', '20'),
('med02', 'manager', 'N#20', 'med_A20', 'Acme', '50', '100', '2015-09-12', '2017-09-12', '100'),
('med02', 'manager', 'A#21', 'Seclo', 'Square', '50', '60', '2017-09-12', '2018-09-12', '10'),
('med02', 'manager', 'A#23', 'med_A23', 'drug', '150', '200', '2015-09-12', '2017-09-12', '20'),
('man03isl', 'Rashib', 'd#03', 'Sedil', 'square', '15', '20', '2015-09-18', '2015-09-18', '20');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
